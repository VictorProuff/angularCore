﻿using angularCore.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace angularCore.Data
{
    public class UserData
    {
		string pathFile = "test.json";

		public void DeleteUserById(string id)
		{
			List<UserModel> listUsers = ReadListAllUsersInFile();
			UserModel userDelete = listUsers.Find(user => user.Id == id);

			if (userDelete != null)
			{
				listUsers.Remove(userDelete);
				string json = JsonConvert.SerializeObject(listUsers, Formatting.Indented);
				File.WriteAllText(pathFile, json);
			}
		}

		public void WriteTextInFile(UserModel user)
		{
			List<UserModel> listUsers = ReadListAllUsersInFile();
			listUsers.Add(user);

			string json = JsonConvert.SerializeObject(listUsers, Formatting.Indented);

			File.WriteAllText(pathFile, json);
		}

		public List<UserModel> ReadListAllUsersInFile()
		{
			using (StreamReader file = File.OpenText(pathFile))
			{
				List<UserModel> listUsers = JsonConvert.DeserializeObject<List<UserModel>>(file.ReadToEnd());

				if (listUsers == null) listUsers = new List<UserModel>();

				return listUsers;
			}

		}
    }
}
