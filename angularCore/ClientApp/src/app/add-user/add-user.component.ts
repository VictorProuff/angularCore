import { Component, Inject, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
})

export class AddUserComponent {
  @Output() someEvent = new EventEmitter<string>();

  constructor(private http: HttpClient,  @Inject('BASE_URL') private baseUrl: string) {

  }

  callParent() {
    this.someEvent.next();
  }

  onSubmitUserDetails(formulaire) {
    this.http.post(this.baseUrl + 'api/User/AddUserInListe', formulaire).subscribe(result => {

      this.callParent();
    }, error => console.error(error));
    
  }

}
