import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-listUser',
  templateUrl: './listUser.component.html'
})
export class ListUserComponent {
  public users: User[];

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this.getListUsers();
  }

  getListUsers() {
    this.http.get<User[]>(this.baseUrl + 'api/User/GetUserList').subscribe(result => {
      this.users = result;
    }, error => console.error(error));

  }

  deleteUserById(userId) {
    console.log("userId", userId);
    this.http.delete(this.baseUrl + 'api/User/' + userId, userId).subscribe(result => {
      this.getListUsers();
    }, error => console.error(error));
  }
}

interface User {
  id: string;
  prenom: string;
  nom: string;
}
