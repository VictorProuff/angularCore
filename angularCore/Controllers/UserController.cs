﻿using angularCore.Data;
using angularCore.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace angularCore.Controllers
{
	[Route("api/[controller]")]
	public class UserController : Controller
    {
		[HttpGet("[action]")]
		public IEnumerable<UserModel> GetUserList()
		{
			UserData userData = new UserData();
			return userData.ReadListAllUsersInFile();
		}

		[HttpDelete("{id}")]
		public ActionResult DeleteUserById(string id)
		{
			UserData userData = new UserData();
			userData.DeleteUserById(id);
			
			return Ok();
		}

		[HttpPost("[action]")]
		public IActionResult AddUserInListe([FromBody] UserModel user)
		{
			user.Id = Guid.NewGuid().ToString("N");

			UserData userData = new UserData();
			userData.WriteTextInFile(user);

			return CreatedAtRoute("GetTodo", new { id = user.Id }, user);
		}

		[HttpGet("{id}", Name = "GetTodo")]
		public ActionResult<UserModel> GetById(string id)
		{
			UserData userData = new UserData();

			UserModel item = userData.ReadListAllUsersInFile().Find(user => user.Id == id);
			if (item == null)
			{
				return NotFound();
			}
			return item;
		}
	}
}
